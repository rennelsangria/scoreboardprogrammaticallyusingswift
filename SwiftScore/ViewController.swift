//
//  ViewController.swift
//  SwiftScore
//
//  Created by Rennel Sangria on 7/7/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {
    
    let label1 = UILabel() as UILabel
    let label2 = UILabel() as UILabel
    let P1Stepper = UIStepper() as UIStepper
    let P2Stepper = UIStepper() as UIStepper
    let P1textField = UITextField() as UITextField
    let P2textField = UITextField() as UITextField
    let resetButton = UIButton() as UIButton
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 1, alpha: 1.0)
        makeLayout()
        
        //dismiss the keyboard
        self.P1textField.delegate = self;
        self.P2textField.delegate = self;
        
    }
    
    
    func makeLayout(){
        
    //make the left view
        let view1 = UIView()
        view1.setTranslatesAutoresizingMaskIntoConstraints(false)
        view1.backgroundColor = UIColor(red: 0.85, green: 0.75, blue: 0.1, alpha: 1.0)
    
    //make the right view
        let view2 = UIView()
        view2.setTranslatesAutoresizingMaskIntoConstraints(false)
        view2.backgroundColor = UIColor(red: 0.75, green: 0.85, blue: 0.1, alpha: 1.0)
        
        view.addSubview(view1)
        view.addSubview(view2)
        
    //add the controls
        
        //add the reset button
        resetButton.setTitle("Reset", forState: UIControlState.Normal)
        resetButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        resetButton.addTarget(self, action: "resetButtonTouched:", forControlEvents: UIControlEvents.TouchUpInside)
        resetButton.setTranslatesAutoresizingMaskIntoConstraints(false)
        resetButton.backgroundColor = UIColor.redColor()
        view.addSubview(resetButton)
        
        
        P1Stepper.wraps = false
        P1Stepper.autorepeat = false
        P1Stepper.maximumValue = 21
        P1Stepper.addTarget(self, action: "P1stepperValueChanged:", forControlEvents: .ValueChanged)
        P1Stepper.setTranslatesAutoresizingMaskIntoConstraints(false)
        view1.addSubview(P1Stepper)
        
        P2Stepper.wraps = false
        P2Stepper.autorepeat = false
        P2Stepper.maximumValue = 21
        P2Stepper.addTarget(self, action: "P2stepperValueChanged:", forControlEvents: .ValueChanged)
        P2Stepper.setTranslatesAutoresizingMaskIntoConstraints(false)
        view2.addSubview(P2Stepper)
        
        label1.backgroundColor = UIColor.blueColor();
        label1.text = "0"
        label1.font = UIFont.systemFontOfSize(22)
        label1.textAlignment = .Center;
        label1.textColor = UIColor.whiteColor();
        label1.setTranslatesAutoresizingMaskIntoConstraints(false)
        view1.addSubview(label1)
        
        P1textField.text = "Player1"
        P1textField.backgroundColor = UIColor.greenColor()
        P1textField.setTranslatesAutoresizingMaskIntoConstraints(false)
        view1.addSubview(P1textField)
        
        P2textField.text = "Player2"
        P2textField.backgroundColor = UIColor.greenColor()
        P2textField.setTranslatesAutoresizingMaskIntoConstraints(false)
        view2.addSubview(P2textField)
        
        label2.backgroundColor = UIColor.blueColor();
        label2.text = "0"
        label2.font = UIFont.systemFontOfSize(22)
        label2.textAlignment = .Center;
        label2.textColor = UIColor.whiteColor();
        label2.setTranslatesAutoresizingMaskIntoConstraints(false)
        view2.addSubview(label2)

        
    //--------------constraints
        
    //make dictionary for views
        let viewsDictionary = ["left":view1, "right":view2, "label1":label1, "label2":label2, "P1Stepper":P1Stepper, "P2Stepper":P2Stepper, "P1textField":P1textField, "P2textField":P2textField,"reset":resetButton]

        
    //set the two columns
        let view_constraint_H:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[left(right)]-[right]-10-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
        let view_constraint_V:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("V:|-20-[right]-20-[reset(50)]-10-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
        let view_constraint_V2:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("V:|-20-[left]-20-[reset(50)]-10-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
        
    //center the reset button
        let reset_constraint_H:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[reset]-10-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
        
    //center the player 1 controls in the left view
        let label1Constraint_V:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("V:[left]-(<=1)-[P1textField]-[label1(80)]-10-[P1Stepper]", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: viewsDictionary)
        
        let label1Constraint_H:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("H:[left]-(<=1)-[label1(85)]", options: NSLayoutFormatOptions.AlignAllCenterY, metrics: nil, views: viewsDictionary)
        
    //center the player 2 controls in the right view
        let label2Constraint_V:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("V:[right]-(<=1)-[P2textField]-[label2(80)]-10-[P2Stepper]", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: viewsDictionary)
        
        let label2Constraint_H:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("H:[right]-(<=1)-[label2(85)]", options: NSLayoutFormatOptions.AlignAllCenterY, metrics: nil, views: viewsDictionary)
        
    //set the width of the p1 textfield
        let P1textFieldWidthConstraint_H:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("H:[P1textField(85)]", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
        
    //set the width of the p2 textfield
        let P2textFieldWidthConstraint_H:NSArray = NSLayoutConstraint.constraintsWithVisualFormat("H:[P2textField(85)]", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
        
    //add the constraints
        view.addConstraints(view_constraint_H as [AnyObject])
        view.addConstraints(view_constraint_V as [AnyObject])
        view.addConstraints(view_constraint_V2 as [AnyObject])
        view.addConstraints(label1Constraint_V as [AnyObject])
        view.addConstraints(label1Constraint_H as [AnyObject])
        view.addConstraints(label2Constraint_V as [AnyObject])
        view.addConstraints(label2Constraint_H as [AnyObject])
        view.addConstraints(reset_constraint_H as [AnyObject])
        view.addConstraints(P1textFieldWidthConstraint_H as [AnyObject])
        view.addConstraints(P2textFieldWidthConstraint_H as [AnyObject])
        
    }
    
    func P1stepperValueChanged(sender:UIStepper!){
        //println("It Works, Value is --&gt;\(Int(sender.value).description)")
        label1.text = (Int(sender.value).description)
    }
    
    func P2stepperValueChanged(sender:UIStepper!){
        //println("It Works, Value is --&gt;\(Int(sender.value).description)")
        label2.text = (Int(sender.value).description)
    }
    //dismiss the keyboard
    func textFieldShouldReturn(P1textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //dismiss the keyboard
    func textField2ShouldReturn(P2textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func resetButtonTouched(sender:UIButton!){
        P1Stepper.value = 0
        label1.text = "0"
        P2Stepper.value = 0
        label2.text = "0"
        //println("It works")
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

